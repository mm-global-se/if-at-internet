# AT-Internet

---

[TOC]

## Overview

This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to __AT-Internet__.

## How we send the data

We are calling `window.xt_mvt()` function passing `'page_test_mvt', '1', xt_abmv, xt_abmvc` as arguments, where:

+ `xt_abmv` - contains campaign name and campaign experience split by `-1-` (according to AT-Internet integration documentation)

    _E.g. `[T1Button]-1-[color:red|shape:round]`;_

+ `xt_abmvc` - contains an array of campaign elements and variants wrapped split by `-`

    _E.g. `[color]-[red], [shape]-[round]`_

## Data Format

The data sent to Kissmetrics will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
)

## Download

* [at-internet-register.js](https://bitbucket.org/mm-global-se/if-at-internet/src/master/src/at-internet-register.js)

* [at-internet-initialize.js](https://bitbucket.org/mm-global-se/if-at-internet/src/master/src/at-internet-initialize.js)

## Prerequisite

+ Campaign name

## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [at-internet-register.js](https://bitbucket.org/mm-global-se/if-at-internet/src/master/src/at-internet-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Google Analytics Register scripts!

+ Create a campaign script and add the [at-internet-initialize.js](https://bitbucket.org/mm-global-se/if-at-internet/src/master/src/at-internet-initialize.js) script. Customise the code by changing the campaign name, account ID (only if needed), and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.

## QA

After the data is successfully sent to AT-Internet you should see a new image requested in Network tab of the developer tools (e.g. Chrome DevTools). It should have status 200.

![2015-04-09_1007.png](https://bitbucket.org/repo/d9pjGj/images/500657265-2015-04-09_1007.png)