mmcore.IntegrationFactory.register(
  'AT-Internet', {

  validate: function (data) {
    if(!data.campaign) return 'No campaign.';
    return true;
  },

  check: function (data) {
    return typeof window.xt_mvt === 'function';
  },

  exec: function (data)  {
    var name = data.campaign,
        experience = data.campaignInfo.replace(name + '=', ''),
        elements = experience.split('|'),
        xt_abmvc = [],
        xt_abmv = '[' + name + ']-1-' + '[' + experience + ']';

    window.xtpage = window.xtpage || "page_test_mvt";
    window.xtn2 = window.xtn2 || "1";

    for (var i = 0; i < elements.length; i ++) {
      var element = elements[i].split(':')[0],
          variant = elements[i].split(':')[1];

      xt_abmvc.push('[' + element + ']-[' + variant + ']');
    }

    xt_mvt('page_test_mvt', '1', xt_abmv, xt_abmvc);
  }
});
